import React, {useEffect, useState} from 'react';
import './Chat.css';
import Message from "../../components/Message/Message";
import MessItem from "../../components/MessItem/MessItem";

const url = "http://146.185.154.90:8000/messages";

const Chat = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    const num = async () => {
      const response = await fetch(url);
      if(response.ok){
        const post = await response.json();
        setPosts(post);
      }
    };
    num().catch(console.error);
  },[]);

  const postMess = async () => {
    const  data = new URLSearchParams();
    data.set('message', 'Hello Turkistan');
    data.set('author', 'Pi Pattel');

    const response = await fetch(url, {
      method: 'post',
      body: data
    })
  }

  return (
      <div className="main-block">
        <Message
            title="Dzhunusov Atakhan"
            postMess={postMess}
        />
        <section className="Posts">
          {posts.map(post => (
              <MessItem
                  key={post.id}
                  message={post.message}
                  author={post.author}
              />
          ))}
        </section>

      </div>
  );
};

export default Chat;