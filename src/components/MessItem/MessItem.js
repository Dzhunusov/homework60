import React from 'react';
import './MessItem.css';

const MessItem = props => {
  return (
      <div className="message-item">
        <h4 className="user-name">{props.author}</h4>
        <p>{props.message}</p>
      </div>
  );
};

export default MessItem;