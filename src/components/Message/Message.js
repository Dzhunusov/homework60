import React from 'react';
import './Message.css';

const Message = props => {
  return (
      <div>
        <h3>{props.title}</h3>
        <input type="text" placeholder="Enter you message" className="mess"/>
        <button onClick={props.postMess} >Send Mess</button>

      </div>
  );
};

export default Message;